# Auto Deploy (Herokuish)

Use the following .gitlab-ci.yml for deploying apps using Herokuish:

```
image: alpine:latest

stages:
  - build
  - test
  - deploy

build:
  stage: build
  image: docker:stable-git
  services:
  - docker:stable-dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - |
        if [[ -f Dockerfile ]]; then
            docker build --pull -t "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA" .
        else
            docker run -i --name="ci_job_build_${CI_JOB_ID}" -v "$(pwd):/tmp/app:ro" gliderlabs/herokuish /bin/herokuish buildpack build
            docker commit "ci_job_build_${CI_JOB_ID}" "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"
            docker rm "ci_job_build_${CI_JOB_ID}" >/dev/null

            docker create --env PORT=5000 --name="ci_job_build_${CI_JOB_ID}" "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA" /bin/herokuish procfile start web
            docker commit "ci_job_build_${CI_JOB_ID}" "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"
            docker rm "ci_job_build_${CI_JOB_ID}" >/dev/null
        fi
    - docker push "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"

test:
  stage: test
  image: gliderlabs/herokuish:latest
  script:
    - cp -R . /tmp/app
    - /bin/herokuish buildpack test
  except:
    variables:
      - $TEST_DISABLED

deploy:
  stage: deploy
  image: registry.gitlab.com/cryptonetes/helm-charts/master
  variables:
    KUBECONFIG: /kube-config
  script:
    - |
        echo ${KUBE_CONFIG} | base64 -d > ${KUBECONFIG}

        if [[ "$CI_PROJECT_VISIBILITY" != "public" ]]; then
          secret_name="$CI_PROJECT_NAME-gitlab-registry"

          kubectl describe secrets $secret_name -n "$DEPLOY_NAMESPACE" || \
          kubectl create secret -n "$DEPLOY_NAMESPACE" \
            docker-registry $secret_name \
            --docker-server="$CI_REGISTRY" \
            --docker-username="${CI_DEPLOY_USER:-$CI_REGISTRY_USER}" \
            --docker-password="${CI_DEPLOY_PASSWORD:-$CI_REGISTRY_PASSWORD}" \
            --docker-email="$GITLAB_USER_EMAIL" \
            -o yaml --dry-run | kubectl create -n "$DEPLOY_NAMESPACE" -f -

        else
          secret_name=''
        fi

        kubectl get secrets "$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME" -n "$DEPLOY_NAMESPACE" || \
        kubectl create secret generic -n "$DEPLOY_NAMESPACE" "$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME"

        helm init --client-only
        helm upgrade --install \
          --wait \
          --set image.repository="$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG" \
          --set image.tag="$CI_COMMIT_SHA" \
          --set image.secrets[0].name="$secret_name" \
          --set application.track="$CI_COMMIT_REF_NAME" \
          --set application.build="$CI_COMMIT_SHA" \
          --set service.url="$DEPLOY_DOMAIN" \
          --set web.enabled=${WEB_ENABLED:-true} \
          --set worker.enabled=${WORKER_ENABLED:-false} \
          --set cronjob.enabled=${CRONJOB_ENABLED:-false} \
          --tiller-namespace="$DEPLOY_NAMESPACE" \
          --namespace="$DEPLOY_NAMESPACE" \
          "$CI_PROJECT_NAME" \
          /charts/auto-deploy

  only:
    - master
```

For each application the following variables need to be created prior to deployment:

- DEPLOY_NAMESPACE
- DEPLOY_DOMAIN
- KUBE_CONFIG (`cat kube-config | base64 -w 0`)

You will need to create a Gitlab deploy token with the exact name: `gitlab-deploy-token`

Optional boolean variables:

- TEST_DISABLED
- WEB_ENABLED
- WORKER_ENABLED
- CRONJOB_ENABLED
