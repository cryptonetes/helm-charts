# Horizen (Zencash) Node

Horizen Nodes help to establish one of the most secure, distributed, and resilient networks,
powering the Horizen ecosystem and offering enhanced privacy as the first to market end-to-end encrypted blockchain network.

This repository builds the following images for deploying on Cryptonetes - a cloud hosted service powered by Kubernetes.

```
helm install --name node --tiller-namespace=cryptonetes-horizen --namespace=cryptonetes-horizen \
    --set configmap.FQDN="cryptonetes.com" \
    --set configmap.STAKEADDR="asd" \
    --set configmap.EMAIL="test@example.com" \
    horizen/
```

Note: This uses the host port to expose the Zend daemon. We use taints and tolerations as part of each deployment.
