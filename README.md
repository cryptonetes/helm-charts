# Helm Charts

This repository contains the helm charts used by Cryptonetes.

## Charts

### auto-deploy

Auto deploy apps built using using Herokuish.

A secret needs to be created and will be automatically deployed
into the deployment to allow for external config management.

### horizen

Deploy a horizen node

### origintrail

Deploy a cluster of trac nodes.
