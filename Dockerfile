FROM alpine:latest

ENV HELM_VERSION=2.6.1

RUN apk add --update --no-cache curl tar gzip bash ca-certificates git && \
    curl -L https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz |tar xvz && \
    mv linux-amd64/helm /usr/bin/ && \
    chmod +x /usr/bin/helm && \
    helm version --client && \
    rm -rf linux-amd64 && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && \
    chmod +x kubectl && \
    mv kubectl /usr/local/bin/kubectl && \
    kubectl version --client && \
    rm -rf /var/cache/apk/* && \
    mkdir /charts

COPY . /charts
